enum Activity {
  move,
  mineFoo,
  mineBar,
  assemble,
  createRobot,
  idle,
}

type RobotProps = {
  id: number;
};

export default class Robot {
  props: RobotProps;
  isSleeping: boolean;
  activity: Activity;

  constructor(props: RobotProps) {
    this.props = props;
    this.activity = Activity.idle;
    this.isSleeping = false;
  }

  move(activity: Activity) {
    return new Promise((resolve, reject) => {
      this.activity = activity;
      this.sleep(5)
        .then((res) => {
          this.activity = activity;
          console.log("Robot " + this.props.id + " a move");
          return resolve(res);
        })
        .catch((err) => {
          // déjà en train de dormir
          return reject(err);
        });
    });
  }

  mineFoo() {
    return new Promise((resolve, reject) => {
      if (this.activity != Activity.mineFoo) {
        this.move(Activity.mineFoo)
          .then((res) => {
            this.sleep(1).then((res) => {
              console.log("Robot " + this.props.id + " vient de miner FOO");
              return resolve("MineFoo");
            });
          })
          .catch((err) => {
            // déjà en train de dormir
            return reject(err);
          });
      } else {
        this.sleep(1)
          .then((res) => {
            console.log("Robot " + this.props.id + " vient de miner FOO");
            return resolve("MineFoo");
          })
          .catch((err) => {
            // déjà en train de dormir
            return reject(err);
          });
      }
    });
  }

  mineBar() {
    return new Promise((resolve, reject) => {
      if (this.activity != Activity.mineBar) {
        this.move(Activity.mineBar)
          .then((res) => {
            // random entre 0.5 et 2
            this.sleep(Math.random() * (2 - 0.5) + 0.5).then((res) => {
              console.log("Robot " + this.props.id + " vient de miner BAR");
              return resolve("MineBar");
            });
          })
          .catch((err) => {
            // déjà en train de dormir
            return reject(err);
          });
      } // random entre 0.5 et 2
      else {
        this.sleep(Math.random() * (2 - 0.5) + 0.5)
          .then((res) => {
            console.log("Robot " + this.props.id + " vient de miner BAR");
            return resolve("MineBar");
          })
          .catch((err) => {
            // déjà en train de dormir
            return reject(err);
          });
      }
    });
  }

  assemble() {
    return new Promise((resolve, reject) => {
      if (this.activity != Activity.assemble) {
        this.move(Activity.assemble).then((res) => {
          this.sleep(2)
            .then((res) => {
              const win = Math.floor(Math.random() * 100) > 59;
              if (win) {
                resolve("Assemble");
              }
              else {
                console.log(
                  "Robot " + this.props.id + " a raté l'assemblage du foobar"
                );
                reject("Fail");
              }
            })
            .catch((err) => {
              return reject(err);
            });
        });
      } else if (this.activity == Activity.assemble) {
        this.sleep(2)
          .then((res) => {
            const win = Math.floor(Math.random() * 100) > 59;
            if (win) {
              console.log(
                "Robot " + this.props.id + " vient d'assembler un foobar"
              );
              return resolve("Assemble");
            } else {
              console.log(
                "Robot " + this.props.id + " a raté l'assemblage du foobar"
              );
              return reject("Fail");
            }
          })
          .catch((err) => {
            // déjà en train de dormir
            return reject(err);
          });
      }
    });
  }

  createRobot() {
    return new Promise((resolve, reject) => {
      if (this.activity != Activity.createRobot) {
        this.move(Activity.createRobot)
          .then((res) => {
            console.log("Robot " + this.props.id + " a créé un robot");
            return resolve("CreateRobot");
          })
          .catch((err) => {
            // déjà en train de dormir
            return reject(err);
          });
      }
    });
  }

  changeSleep() {
    this.isSleeping = true;
    console.log(this.isSleeping);
  }

  sleep(duration: number) {
    return new Promise((resolve, reject) => {
      if (this.isSleeping) {
        return reject("Already sleeping");
      }
      this.isSleeping = true;
      setTimeout(() => {
        this.isSleeping = false;
        return resolve("Ready");
      }, duration * 1000);
    });
  }
}
