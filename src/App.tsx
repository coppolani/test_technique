import { useEffect, useState } from "react";

import "./App.css";
import Robot from "./components/Robot.ts";
import Robots from "./components/Robots.tsx";

const App: React.FC = () => {
  const [robotArray, setRobot] = useState<Robot[]>([
    new Robot({ id: 1 }),
    new Robot({ id: 2 }),
  ]);

  const [foo, setFoo] = useState(0);
  const [bar, setBar] = useState(0);
  const [fooBar, setFoobar] = useState(0);

  useEffect(() => {
    if (robotArray.length < 20) {
      if (robotArray.length == 2) {
        if (fooBar >= 3 && foo >= 6) {
          createRobot(robotArray[0]);
        }
        if (fooBar >= 3 && foo < 6) {
          mineFoo(robotArray[0]);
        }
        if (foo >= 6 && bar >= 6) {
          mineFoo(robotArray[0]);
          if (bar > 0) {
            assemble(robotArray[1]);
          }
          if (foo == 0 || bar == 0) {
            mineBar(robotArray[1]);
          }
        }
        if (foo >= 0 && bar >= 0) {
          mineFoo(robotArray[0]);
          mineBar(robotArray[1]);
        }
      }
      if (robotArray.length >= 3) {
        if (fooBar >= 3 && foo >= 6) {
          createRobot(robotArray[0]);
        }
        if (fooBar >= 3 && foo < 6) {
          mineFoo(robotArray[0]);
        }
        if (foo >= 6 && bar >= 6) {
          mineFoo(robotArray[0]);
          if (bar > 0) {
            assemble(robotArray[1]);
            if (robotArray.length >= 4) {
              assemble(robotArray[3]);
            }
            if (robotArray.length >= 5) {
              assemble(robotArray[4]);
            }
            if (robotArray.length >= 6) {
              assemble(robotArray[5]);
            }

          }
          if (foo == 0 || bar == 0) {
            mineBar(robotArray[1]);
          }
        }
        if (foo >= 0 && bar >= 0) {
          mineFoo(robotArray[0]);
          mineBar(robotArray[1]);
          mineBar(robotArray[2]);
        }
      }
    }
  }, [foo, bar, fooBar, robotArray.length]);


  const mineBar = (robot: Robot) => {
    return new Promise((resolve, reject) => {
      robot
        .mineBar()
        .then((res) => {
          setBar(bar + 1);
          return resolve("ok");
        })
        .catch((err) => {
        });
    });
  };

  const mineFoo = (robot: Robot) => {
    return new Promise((resolve, reject) => {
      robot
        .mineFoo()
        .then((res) => {
          setFoo(foo + 1);
          return resolve('ok');
        })
        .catch((err) => {
        });
    });
  };

  const createRobot = (robot: Robot) => {
    return new Promise((resolve, reject) => {
      if (foo >= 6 && fooBar >= 3) {
        robot
          .createRobot()
          .then((res) => {
            setFoo(foo - 6);
            setFoobar(fooBar - 3);
            const id = robotArray.length + 1;
            setRobot(oldArray => [...oldArray, new Robot({ id })]);
            return resolve("ok");
          })
          .catch((err) => {
            if (err == "Fail.") setFoo(foo - 1);
          });
      }
    });
  };

  const assemble = (robot: Robot) => {
    return new Promise((resolve, reject) => {
      if (foo >= 1 && bar >= 1) {
        robot
          .assemble()
          .then((res) => {
            setFoo(foo - 1);
            setBar(bar - 1);
            setFoobar(fooBar + 1);
            return resolve('ok');
          })
          .catch((err) => {
            if (err == "Fail.") {
              setFoo(foo - 1);
            }
          });
      }
    });
  };

  return (
    <div className="App">

      <ul className="list">
        <li>
          foo : {foo}
        </li>
        <li>
          bar : {bar}
        </li>
        <li>
          foobar : {fooBar}
        </li>
        <li>
          number of Robots : {robotArray.length}
        </li>
      </ul>
    </div>
  );
};

export default App;
